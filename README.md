# Análisis de Accidentes de Tránsito Fatales en Lima Metropolitana

En este proyecto se muestran los resultados del análisis del Censo Nacional de Comisarías 2017 (data pública) realizado con el software libre R. El objetivo fue identificar factores de influencia en los accidentes de tránsito en Lima Metropolitana.